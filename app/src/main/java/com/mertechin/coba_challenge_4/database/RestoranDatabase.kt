package com.example.simpleroom

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.mertechin.coba_challenge_4.database.RestoranDAO
import com.mertechin.coba_challenge_4.dataclass.Restoran


@Database(entities = [Restoran::class], version = 4, exportSchema = false)
abstract class RestoranDatabase : RoomDatabase() {

	abstract val restoranDao: RestoranDAO

	companion object {

		@Volatile
		private var INSTANCE: RestoranDatabase? = null

		fun getInstance(context: Context): RestoranDatabase {
			synchronized(this) {
				var instance = INSTANCE

				if (instance == null) {
					instance = Room.databaseBuilder(
						context.applicationContext,
						RestoranDatabase::class.java,
						"db_restoran.db"
					)
						.fallbackToDestructiveMigration()
						.allowMainThreadQueries()
						.build()
					INSTANCE = instance
				}
				return instance
			}
		}
	}
}
