package com.mertechin.coba_challenge_4.database

import androidx.lifecycle.LiveData
import androidx.room.*
import com.mertechin.coba_challenge_4.dataclass.Restoran

@Dao
interface RestoranDAO {
	@Insert
	fun insert(restoran: Restoran)

	@Update
	fun update(restoran: Restoran)

	@Query("DELETE FROM tbl_keranjang")
	fun deleteAll()

	@Query("SELECT SUM(price) FROM tbl_keranjang")
	fun measure():Int

	@Query("SELECT * FROM tbl_keranjang")
	fun getAllCart(): LiveData<List<Restoran>>
}