package com.mertechin.coba_challenge_4.ui.dashboard

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mertechin.challenge4.MenuAdapter
import com.mertechin.coba_challenge_4.MainActivity
import com.mertechin.coba_challenge_4.R
import com.mertechin.coba_challenge_4.databinding.FragmentDashboardBinding
import com.mertechin.coba_challenge_4.dataclass.data_menu
import com.mertechin.coba_challenge_4.ui.detail.DetailActivity

class DashboardFragment : Fragment() {
	private var isGrid = true
	private lateinit var viewModel: DashboardViewModel
	private lateinit var menuAdapter: MenuAdapter
	private lateinit var sharedPreferences: SharedPreferences
	private lateinit var editor: SharedPreferences.Editor
	private val NILAI_GRID = "IS_GRID"
	private var _binding: FragmentDashboardBinding? = null

	// This property is only valid between onCreateView and
	// onDestroyView.
	private val binding get() = _binding!!

	override fun onCreateView(
		inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
	): View {
		sharedPreferences = requireContext().getSharedPreferences("spGrid", Context.MODE_PRIVATE)
		editor = sharedPreferences.edit()
		isGrid = sharedPreferences.getBoolean(NILAI_GRID, true)

		viewModel = ViewModelProvider(this).get(DashboardViewModel::class.java)

		_binding = FragmentDashboardBinding.inflate(inflater, container, false)
		// Set up RecyclerView
		menuAdapter = MenuAdapter(isGrid, getData())

		setupRecyclerView(isGrid)
		setupActionChangeLayout()

		val root: View = binding.root
		return root
	}

	override fun onDestroyView() {
		super.onDestroyView()
		_binding = null
	}

	 fun setupRecyclerView(isGrid: Boolean) {
		val rvData = binding.rvData
		val change = binding.gantiLayout

		menuAdapter.setOnItemClickCallback(object : MenuAdapter.OnItemClickCallback {
			override fun onItemClicked(data: data_menu) {
				val intentDetail = Intent(requireContext(), DetailActivity::class.java)
				intentDetail.putExtra("DATA", data)
				startActivity(intentDetail)
			}
		})

		rvData.adapter = menuAdapter

		if (isGrid) {
			rvData.layoutManager = GridLayoutManager(requireContext(), 2)
			change.setImageResource(R.drawable.ic_baseline_apps_24)
		} else {
			rvData.layoutManager = LinearLayoutManager(requireContext())
			change.setImageResource(R.drawable.ic_baseline_format_list_bulleted_24)
		}
	}

	 fun setupActionChangeLayout() {
		val change = binding.gantiLayout
		change.setOnClickListener {
			isGrid = !isGrid
			setupRecyclerView(isGrid)
			editor.putBoolean(NILAI_GRID, isGrid)
			editor.apply()
		}
	}

	private fun getData() : ArrayList<data_menu> {
		val photo = resources.obtainTypedArray(R.array.data_photo)
		val name = resources.getStringArray(R.array.data_nama)
		val price = resources.getIntArray(R.array.data_harga)
		val desc = resources.getStringArray(R.array.data_deskripsi)
		val menuList = ArrayList<data_menu>()
		for (i in name.indices) {
			val menu = data_menu(photo.getResourceId(i, -1), name[i], price[i], desc[i])
			menuList.add(menu)
		}

		return menuList
	}
}