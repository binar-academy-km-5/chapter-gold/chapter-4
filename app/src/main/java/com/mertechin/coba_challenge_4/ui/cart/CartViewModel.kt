package com.mertechin.coba_challenge_4.ui.cart

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.simpleroom.RestoranDatabase
import com.mertechin.coba_challenge_4.dataclass.Restoran
import com.mertechin.coba_challenge_4.dataclass.data_cart

class CartViewModel: ViewModel() {
	fun getCart(context: Context): LiveData<List<Restoran>>{
		val storage = RestoranDatabase.getInstance(context).restoranDao
		return storage.getAllCart()
	}

	fun getSUM(context: Context): Int {
		val storage = RestoranDatabase.getInstance(context).restoranDao
		val totalHarga: Int = storage.measure()
		return totalHarga
	}
}