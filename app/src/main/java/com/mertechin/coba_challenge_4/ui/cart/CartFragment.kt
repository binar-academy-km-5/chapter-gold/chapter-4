package com.mertechin.coba_challenge_4.ui.cart

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.mertechin.coba_challenge_4.MainActivity
import com.mertechin.coba_challenge_4.adapter.CartAdapter
import com.mertechin.coba_challenge_4.databinding.FragmentCartBinding
import com.mertechin.coba_challenge_4.dataclass.Restoran
import com.mertechin.coba_challenge_4.ui.checkout.CheckoutActivity
import com.mertechin.coba_challenge_4.ui.detail.DetailActivity

class CartFragment : Fragment() {

	private var _binding: FragmentCartBinding? = null
	private lateinit var cartAdapter: CartAdapter

	// This property is only valid between onCreateView and
	// onDestroyView.
	private val binding get() = _binding!!

	override fun onCreateView(
		inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
	): View {

		_binding = FragmentCartBinding.inflate(inflater, container, false)

		val rvCart = binding.rvCart
		rvCart.layoutManager = LinearLayoutManager(requireContext())

		val cartViewModel = ViewModelProvider(this).get(CartViewModel::class.java)
		cartViewModel.getCart(requireContext()).observe(viewLifecycleOwner) { data ->
			// Inisialisasi adapter dan set adapter setelah data tersedia
			cartAdapter = CartAdapter(data)
			rvCart.adapter = cartAdapter
		}

		binding.tvHargaTotal.text = cartViewModel.getSUM(requireContext()).toString()
		binding.btnCheckout.setOnClickListener{
			// Ambil data untuk ditampilkan ke GMaps
			val intent = Intent(requireContext(),CheckoutActivity::class.java)
			startActivity(intent)
		}

		val root: View = binding.root
		return root
	}

	override fun onDestroyView() {
		super.onDestroyView()
		_binding = null
	}
}