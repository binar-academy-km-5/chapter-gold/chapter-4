package com.mertechin.coba_challenge_4.ui.dashboard

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.mertechin.coba_challenge_4.dataclass.data_menu

class DashboardViewModel : ViewModel() {
	private val _menuList = MutableLiveData<List<data_menu>>()
	val menuList: LiveData<List<data_menu>> get() = _menuList

	init {
		_menuList.value = mutableListOf() // Isi dengan data default atau panggil getData() di sini jika diperlukan.
	}

	fun setMenuList(menuList: List<data_menu>) {
		_menuList.value = menuList
	}
}