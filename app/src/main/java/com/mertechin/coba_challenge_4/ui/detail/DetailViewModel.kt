package com.mertechin.coba_challenge_4.ui.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.mertechin.coba_challenge_4.dataclass.data_menu

class DetailViewModel : ViewModel() {
	private val _data = MutableLiveData<data_menu>()
	val data: LiveData<data_menu> get() = _data

	fun setData(data: data_menu) {
		_data.value = data
	}
}