package com.mertechin.coba_challenge_4.ui.checkout

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.mertechin.coba_challenge_4.R
import com.mertechin.coba_challenge_4.adapter.CartAdapter
import com.mertechin.coba_challenge_4.databinding.ActivityCheckoutBinding
import com.mertechin.coba_challenge_4.dataclass.Restoran
import com.mertechin.coba_challenge_4.ui.cart.CartViewModel
import com.mertechin.coba_challenge_4.ui.success.SuccessActivity

class CheckoutActivity : AppCompatActivity() {
	private lateinit var binding: ActivityCheckoutBinding
	private lateinit var cartAdapter: CartAdapter

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)

		binding = ActivityCheckoutBinding.inflate(layoutInflater)
		setContentView(binding.root)

		val rvCheckout = binding.rcvCheckout
		rvCheckout.layoutManager = LinearLayoutManager(this)

		val checkOutViewModel = ViewModelProvider(this).get(CheckoutViewModel::class.java)
		checkOutViewModel.getCart(this).observe(this) { data ->
			// Inisialisasi adapter dan set adapter setelah data tersedia
			cartAdapter = CartAdapter(data)
			rvCheckout.adapter = cartAdapter
		}

		binding.tvTotalCheckout.text = checkOutViewModel.getSUM(this).toString()
		binding.btnPesan.setOnClickListener{
			// Hapus Data di Database
			checkOutViewModel.deleteData(this)

			val intent = Intent(this,SuccessActivity::class.java)
			startActivity(intent)
		}

	}
}