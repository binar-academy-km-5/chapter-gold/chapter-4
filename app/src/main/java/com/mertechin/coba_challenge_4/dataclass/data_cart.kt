package com.mertechin.coba_challenge_4.dataclass

import androidx.room.ColumnInfo

data class data_cart(
	val img: Int,
	val name: String,
	val price: Int,
	val quantity: Int,
	val note: String
)
